from rest_framework import serializers
from . models import member_profile
class valueSerializer(serializers.Serializer):
    X = serializers.IntegerField()
    N = serializers.IntegerField()

class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = member_profile
        fields = ('id','email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = member_profile(
            email=validated_data['email'],
        )
        user.set_password(validated_data['password'])
        user.save()
        return user