from django.shortcuts import render
from .serializers import valueSerializer, RegisterSerializer
from django.core import serializers
from rest_framework import generics
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser
from rest_framework.authentication import BasicAuthentication
from rest_framework import generics, permissions
from rest_framework.permissions import IsAuthenticated
import json

# Create your views here.
def index(request):
    return render(request , 'index.html')

def math_function(x,n):
    sum = 0
    for i in range(1,n+1):
        sum = sum+ 1/(x**i)
    return sum

class Calc_API(generics.GenericAPIView):
    permission_classes = [permissions.IsAuthenticated]
    authentication_classes= [BasicAuthentication]
    serializer_class = valueSerializer

    def post(self, request, format=None):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer= serializer.__dict__
        print(type(serializer))
        
        
        x=  serializer['_validated_data']['X']
        n=  serializer['_validated_data']['N']
        result = math_function(x,n)

        

        return Response({

            
            "result": result


        })

class RegisterAPI(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": RegisterSerializer(user, context=self.get_serializer_context()).data,
        })
    