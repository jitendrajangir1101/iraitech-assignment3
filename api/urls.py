from django.urls import path
from . import views

urlpatterns = [
    
    path('api/v1/calculate' ,views.Calc_API.as_view() , name = 'calculate'),
    path('', views.RegisterAPI.as_view(), name='register'),
]