from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser ,UserManager ,BaseUserManager


    
class member_profile(AbstractUser): 
    email = models.EmailField( unique = True)  
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    def __str__(self): 
       return "{}".format(self.email)